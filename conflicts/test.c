#define _GNU_SOURCE
#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/time.h>
#include <sys/mman.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <assert.h>
#include <libpmem.h>
#include <linux/mman.h>

#ifdef __x86_64__
#define rdtscll(val) {                                           \
       unsigned int __a,__d;                                        \
       asm volatile("rdtsc" : "=a" (__a), "=d" (__d));              \
       (val) = ((unsigned long)__a) | (((unsigned long)__d)<<32);   \
}
#else
#define rdtscll(val) __asm__ __volatile__("rdtsc" : "=A" (val))
#endif

#define TRASHING_CACHE_LINE_SIZE 64
#define GB ((size_t)1024*1024*1024)
#define HUGE_PAGE_SIZE (1LU*1024*1024*1024)
#define LARGE_PAGE_SIZE (2LU*1024*1024)
#define SIZE (120LU*GB)


static inline void mfence() {
    asm volatile("sfence":::"memory");
}

static inline void clflush(char *data, size_t len) {
   volatile char *ptr = (char *)((unsigned long)data &~(TRASHING_CACHE_LINE_SIZE-1));
   assert(ptr == data);
   mfence();
   for(; ptr<&data[len]; ptr+=TRASHING_CACHE_LINE_SIZE){
      asm volatile("clflush %0" : "+m" (*(volatile char *)ptr));
   }
   mfence();
}

int main(int argc, char** argv) {
    printf("/!\\ Taskset on node 0 if huge pages have been reserved on node 0!\n");

    /* Take all the GB pages */
    char *data;
    data = mmap(NULL, SIZE, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS | MAP_HUGETLB | MAP_HUGE_1GB, 0, 0);
    if(data == MAP_FAILED)
       printf("Cannot allocate gigantic pages, do echo 60 > /sys/devices/system/node/node0/hugepages/hugepages-1048576kB/nr_hugepages\n");

    /* Prefault GB pages by memsetting the first 2MB */
    size_t nb_segments = SIZE/HUGE_PAGE_SIZE;
    for(size_t i = 0; i < nb_segments; i++)
       memset(&data[i*HUGE_PAGE_SIZE], 0, LARGE_PAGE_SIZE);

    /* Random data to copy */
    void *tmp = aligned_alloc(LARGE_PAGE_SIZE, LARGE_PAGE_SIZE);
    memset(tmp, rand(), LARGE_PAGE_SIZE);

    /* Find conflicting pages */
    uint64_t a, b, c, d;
    size_t *conflicts = calloc(nb_segments*nb_segments, sizeof(*conflicts));
    for(size_t i = 0; i < nb_segments; i++) {
       for(size_t j = 0; j < nb_segments; j++) {
          int retries = 0;
retry:
          pmem_memcpy_persist(&data[i*HUGE_PAGE_SIZE], tmp, LARGE_PAGE_SIZE);
          pmem_memcpy_persist(&data[j*HUGE_PAGE_SIZE], tmp, LARGE_PAGE_SIZE);

          mfence();
          rdtscll(a);
          mfence();
          pmem_memcpy_persist(&data[i*HUGE_PAGE_SIZE], tmp, LARGE_PAGE_SIZE);
          mfence();
          rdtscll(b);
          mfence();

          mfence();
          rdtscll(c);
          mfence();
          pmem_memcpy_persist(&data[j*HUGE_PAGE_SIZE], tmp, LARGE_PAGE_SIZE);
          mfence();
          rdtscll(d);
          mfence();

          printf("[%lu, %lu] %lu vs %lu\n", i, j, b-a, d-c);

          if(b-a < 1000000 && d-c < 1000000) {
             conflicts[i*nb_segments+j] = 0;
          } else if(retries < 5) {
             retries++;
             goto retry;
          } else {
             conflicts[i*nb_segments+j] = 1;
          }
       }
    }
    for(size_t i = 0; i < nb_segments; i++) {
       for(size_t j = 0; j < nb_segments; j++) {
          //printf("%lu ", conflicts[i*nb_segments+j]);
          if(conflicts[i*nb_segments+j] != conflicts[j*nb_segments+i])
             printf("Inconsistency %lu %lu\n", i, j);
       }
       printf("\n");
    }

    return 0;
}
