#!/usr/bin/perl
use strict;
use warnings;

my $cmd = "sudo ../tmp/pagemap/pagemap ".join(" ", @ARGV);
print "$cmd\n";
my $res = `$cmd`;

my $huge = 2*1024*1024;
my ($contiguous_pages, $total_pages, $current_is_split);

my ($last_virt, $last_phys);
my @lines = split(/\n/, $res);
for my $l (@lines) {
   #0x7f10b8fff000     : pfn 9a663ff          soft-dirty 1 file/shared 0 swapped 0 present 1
   print $l."\n";
   if($l =~ /0x([a-f0-9]+).* pfn ([a-f0-9]+)/) {
      my $virt = hex($1);
      my $phys = hex($2);
      if($virt % $huge == 0) {
         $total_pages++;
         $contiguous_pages++ if(!$current_is_split);
         $current_is_split = 0;
      } else {
         my $diff = $virt - $last_virt;
         if($diff != 4*1024) {
            print "[$virt, $phys] Unexpected diff in virtual address ($last_virt, $virt, ".($virt-$last_virt).")!\n";
         }

         $diff = $phys - $last_phys;
         if($diff != 1) {
            print "[$virt, $phys] Page is not contiguous in physical memory!\n";
            $current_is_split = 1;
         }
      }
      $last_virt = $virt;
      $last_phys = $phys;
   }
}

