#define _GNU_SOURCE
#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/time.h>
#include <sys/mman.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>


/*
root@labos:/home/blepers/msr# echo 0 > /sys/kernel/mm/transparent_hugepage/khugepaged/scan_sleep_millisecs
root@labos:/home/blepers/msr# htop
root@labos:/home/blepers/msr# cat /sys/kernel/mm/transparent_hugepage/khugepaged/max_ptes_none
*/

#define PAGE_SIZE (4*1024)
#define HUGE_PAGE_SIZE (512LU*PAGE_SIZE)

#define GB (1LU*1024*1024*1024)
//#define SIZE (10*HUGE_PAGE_SIZE)
#define SIZE (1000LU*HUGE_PAGE_SIZE)

#define declare_timer uint64_t elapsed; \
   struct timeval st, et;

#define start_timer gettimeofday(&st,NULL);

#define stop_timer(msg, args...) ;do { \
   gettimeofday(&et,NULL); \
   elapsed = ((et.tv_sec - st.tv_sec) * 1000000) + (et.tv_usec - st.tv_usec) + 1; \
   printf("(%s,%d) [%6lums] " msg "\n", __FUNCTION__ , __LINE__, elapsed/1000, ##args); \
} while(0)

#define declare_perf_stat uint64_t ___s, ___e;


#define start_perf_stat(events); do { \
   char *__cmd; \
   asprintf(&__cmd, "sudo /home/blepers/linux/tools/perf/perf stat -a -e %s &", events); \
   system(__cmd); \
   free(__cmd); \
   sleep(2); \
   rdtscll(___s); \
} while(0);

#define stop_perf_stat(msg, args...) ;do { \
   rdtscll(___e); \
   system("sudo killall -INT -w perf"); \
   printf("(%s,%d) [%15lu cycles %6lu ms] " msg "\n", __FUNCTION__ , __LINE__, ___e - ___s, (___e - ___s)/2100000LU, ##args); \
} while(0)

/*
#define start_perf_stat(events); do { \
   char *__cmd; \
   asprintf(&__cmd, "sudo /home/blepers/linux/tools/perf/perf record -a -g &"); \
   system(__cmd); \
   free(__cmd); \
   sleep(2); \
   rdtscll(___s); \
} while(0);

#define stop_perf_stat(msg, args...) ;do { \
   rdtscll(___e); \
   system("sudo killall -INT -w perf"); \
   char *__cmd; \
   asprintf(&__cmd, "sudo mv perf.data perf.data." msg); \
   system(__cmd); \
   printf("(%s,%d) [%15lu cycles %6lu ms] " msg "\n", __FUNCTION__ , __LINE__, ___e - ___s, (___e - ___s)/2100000LU, ##args); \
} while(0)
*/


#define die(msg, args...) do { \
   printf("(%s,%d) " msg "\n", __FUNCTION__ , __LINE__, ##args); \
   fflush(stdout); \
   exit(-1); \
} while(0)

#ifdef __x86_64__
#define rdtscll(val) {                                           \
       unsigned int __a,__d;                                        \
       asm volatile("rdtsc" : "=a" (__a), "=d" (__d));              \
       (val) = ((unsigned long)__a) | (((unsigned long)__d)<<32);   \
}
#else
#define rdtscll(val) __asm__ __volatile__("rdtsc" : "=A" (val))
#endif

//static __uint128_t g_lehmer64_state;
//static uint64_t lehmer64() {
//g_lehmer64_state *= 0xda942042e4dd58b5;
//return g_lehmer64_state >> 64;
//}

void shuffle(size_t *array, size_t n) {
   if (n > 1) {
      size_t i;
      for (i = 0; i < n - 1; i++) {
         size_t j = i + rand() / (RAND_MAX / (n - i) + 1);
         size_t t = array[j];
         array[j] = array[i];
         array[i] = t;
      }
   }
}

int main() {
   declare_perf_stat;

   //char *a;
   //posix_memalign((void**)&a, HUGE_PAGE_SIZE, SIZE);
   //if(madvise(a, SIZE, MADV_HUGEPAGE))
   //printf("Error!\n");

   /*
    * Create a large file with 4K pages
    */
   //char *path = "/pmem0/test";
   char *path = "/pmem4/test";
   int fd = open(path,  O_RDWR | O_CREAT | O_DIRECT, 0777);
   if(fd == -1)
      die("Cannot open %s\n", path);
   posix_fallocate(fd, 0, SIZE);
   char *a = mmap(NULL, SIZE, PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0);
   if(madvise(a, SIZE, MADV_NOHUGEPAGE))
      printf("Error!\n");
   memset(a, 0, SIZE/2);
   //if(madvise(a, SIZE/2, MADV_HUGEPAGE))
   //printf("Error!\n");
   //if(madvise(&a[SIZE/2], SIZE/2, MADV_NOHUGEPAGE))
   //printf("Error!\n");
   memset(&a[SIZE/2], 0, SIZE/2);

   /*
    * Create a random order of poking in the file
    */
   size_t nb_huge_pages = SIZE/HUGE_PAGE_SIZE;
   size_t *huge_order = malloc(nb_huge_pages * sizeof(*huge_order));
   for(size_t i = 0; i < nb_huge_pages; i++)
      huge_order[i] = i;
   shuffle(huge_order, nb_huge_pages);

   size_t nb_sub_pages = HUGE_PAGE_SIZE/PAGE_SIZE;
   size_t *sub_order = malloc(nb_sub_pages * sizeof(*sub_order));
   for(size_t i = 0; i < nb_sub_pages; i++)
      sub_order[i] = i;
   shuffle(sub_order, nb_sub_pages);


   //const char *evts = "dtlb_store_misses.walk_completed,dtlb_store_misses.walk_completed_2m_4m,dtlb_store_misses.walk_completed_4k"; // for some reason only the 4k event works, but that's good enough
   const char *evts = "page-faults,dTLB-stores,dTLB-store-misses,dtlb_store_misses.walk_completed,dtlb_store_misses.walk_completed_2m_4m,dtlb_store_misses.walk_completed_4k,dTLB-loads,dtlb_load_misses.walk_completed_4k"; // for some reason only the 4k event works, but that's good enough

   /*
    * Random walk on 4K
    */
   size_t total = 0;
   start_perf_stat(evts) {
      for(size_t i = 0; i < 10; i++) {
         for(size_t k = 0; k < nb_huge_pages; k++) {
            for(size_t l = 0; l < nb_sub_pages; l++) {
               //a[huge_order[k]*HUGE_PAGE_SIZE + sub_order[l]*PAGE_SIZE] = i;
               total += a[huge_order[k]*HUGE_PAGE_SIZE + sub_order[l]*PAGE_SIZE];
            }
         }
      }
   } stop_perf_stat("4K");

   start_perf_stat(evts) {
      for(size_t i = 0; i < 10; i++) {
         for(size_t k = 0; k < nb_huge_pages; k++) {
            for(size_t l = 0; l < nb_sub_pages; l++) {
               total += a[huge_order[k]*HUGE_PAGE_SIZE + sub_order[l]*PAGE_SIZE];
            }
         }
      }
   } stop_perf_stat("4K"); // redo just to be sure that perf counters are consistent

   char *cmd;
   asprintf(&cmd, "./parse.pl %ld %p %p > log_4k", (long) getpid(), a, &a[SIZE]);
   system(cmd);

   /*
    * Random walk on (hopefully) 2MB
    */
   if(madvise(a, SIZE, MADV_HUGEPAGE))
      printf("Error!\n");
   memset(a, 0, SIZE);

   start_perf_stat(evts) {
      for(size_t i = 0; i < 10; i++) {
         for(size_t k = 0; k < nb_huge_pages; k++) {
            for(size_t l = 0; l < nb_sub_pages; l++) {
               total += a[huge_order[k]*HUGE_PAGE_SIZE + sub_order[l]*PAGE_SIZE];
            }
         }
      }
   } stop_perf_stat("2M");

   start_perf_stat(evts) {
      for(size_t i = 0; i < 10; i++) {
         for(size_t k = 0; k < nb_huge_pages; k++) {
            for(size_t l = 0; l < nb_sub_pages; l++) {
               total += a[huge_order[k]*HUGE_PAGE_SIZE + sub_order[l]*PAGE_SIZE];
            }
         }
      }
   } stop_perf_stat("2M");

   asprintf(&cmd, "./parse.pl %ld %p %p > log_2M", (long) getpid(), a, &a[SIZE]);
   system(cmd);


   sleep(50);

   start_perf_stat(evts) {
      for(size_t i = 0; i < 10; i++) {
         for(size_t k = 0; k < nb_huge_pages; k++) {
            for(size_t l = 0; l < nb_sub_pages; l++) {
               total += a[huge_order[k]*HUGE_PAGE_SIZE + sub_order[l]*PAGE_SIZE];
            }
         }
      }
   } stop_perf_stat("2M");
}

