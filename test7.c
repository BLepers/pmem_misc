#define _GNU_SOURCE
#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/time.h>
#include <sys/mman.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <assert.h>
#include <linux/mman.h>

#ifdef __x86_64__
#define rdtscll(val) {                                           \
       unsigned int __a,__d;                                        \
       asm volatile("rdtsc" : "=a" (__a), "=d" (__d));              \
       (val) = ((unsigned long)__a) | (((unsigned long)__d)<<32);   \
}
#else
#define rdtscll(val) __asm__ __volatile__("rdtsc" : "=A" (val))
#endif

#define TRASHING_CACHE_LINE_SIZE 64
#define GB ((size_t)1024*1024*1024)
#define HUGE_PAGE_SIZE (1LU*1024*1024*1024)
#define LARGE_PAGE_SIZE (2LU*1024*1024)
#define SIZE (2LU*GB)


static inline void mfence() {
    asm volatile("sfence":::"memory");
}

static inline void clflush(char *data, size_t len) {
   volatile char *ptr = (char *)((unsigned long)data &~(TRASHING_CACHE_LINE_SIZE-1));
   assert(ptr == data);
   mfence();
   for(; ptr<&data[len]; ptr+=TRASHING_CACHE_LINE_SIZE){
      asm volatile("clflush %0" : "+m" (*(volatile char *)ptr));
   }
   mfence();
}

void* reserve(size_t bytes) {
   int flags = MAP_PRIVATE | MAP_NORESERVE | MAP_ANONYMOUS;
   int prot = PROT_NONE;
   void* addr = mmap(NULL, bytes, prot, flags, -1, 0);
   return addr;
}

int main(int argc, char** argv) {
    char *data;
    data = mmap(NULL, SIZE, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS | MAP_HUGETLB | MAP_HUGE_1GB, 0, 0);
    if(data == MAP_FAILED)
       printf("Cannot allocate gigantic pages, do echo 60 > /sys/devices/system/node/node0/hugepages/hugepages-1048576kB/nr_hugepages\n");
    memset(data, 0, SIZE);

    void *addr = reserve(3*GB);
    void *new = mremap(data, GB, GB, MREMAP_MAYMOVE|MREMAP_FIXED, addr); // fails
    printf("%p %p %p %p\n", data, addr, new, MAP_FAILED);

    return 0;
}
