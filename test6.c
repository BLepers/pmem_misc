#define _GNU_SOURCE
#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/time.h>
#include <sys/mman.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <assert.h>
#include <linux/mman.h>

#ifdef __x86_64__
#define rdtscll(val) {                                           \
       unsigned int __a,__d;                                        \
       asm volatile("rdtsc" : "=a" (__a), "=d" (__d));              \
       (val) = ((unsigned long)__a) | (((unsigned long)__d)<<32);   \
}
#else
#define rdtscll(val) __asm__ __volatile__("rdtsc" : "=A" (val))
#endif

#define TRASHING_CACHE_LINE_SIZE 64
#define GB ((size_t)1024*1024*1024)
#define HUGE_PAGE_SIZE (1LU*1024*1024*1024)
#define LARGE_PAGE_SIZE (2LU*1024*1024)
#define SIZE (50LU*GB)


static inline void mfence() {
    asm volatile("sfence":::"memory");
}

static inline void clflush(char *data, size_t len) {
   volatile char *ptr = (char *)((unsigned long)data &~(TRASHING_CACHE_LINE_SIZE-1));
   assert(ptr == data);
   mfence();
   for(; ptr<&data[len]; ptr+=TRASHING_CACHE_LINE_SIZE){
      asm volatile("clflush %0" : "+m" (*(volatile char *)ptr));
   }
   mfence();
}

int main(int argc, char** argv) {
    printf("/!\\ Taskset on node 0 if huge pages have been reserved on node 0!\n");

    char *data;
    data = mmap(NULL, SIZE, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS | MAP_HUGETLB | MAP_HUGE_1GB, 0, 0);
    if(data == MAP_FAILED)
       printf("Cannot allocate gigantic pages, do echo 60 > /sys/devices/system/node/node0/hugepages/hugepages-1048576kB/nr_hugepages\n");
    memset(data, 0, SIZE);

    //char *cmd;
    //asprintf(&cmd, "sudo ../tmp/pagemap/pagemap %ld %p %p > log_memory_1GB", (long) getpid(), data, &data[SIZE]);
    //system(cmd);

    size_t nb_segments = SIZE/HUGE_PAGE_SIZE;
    memset(data, rand(), HUGE_PAGE_SIZE);


    size_t conflict = 0;
    for(size_t i = 0; i < nb_segments; i++) {
       uint64_t a, b, c, d;

       for(size_t k = 0; k < 3; k++) {
          rdtscll(a);
          memset(&data[i*HUGE_PAGE_SIZE], rand(), HUGE_PAGE_SIZE);
          rdtscll(b);

          rdtscll(c);
          memset(&data[0], rand(), HUGE_PAGE_SIZE);
          rdtscll(d);
          printf("GB[0] %lu %p GB[%lu]\t%lu\t%p\n", d-c, &data[0], i, b-a, &data[i*HUGE_PAGE_SIZE]);

          if(d - c > 1700000000LU) {
             if(conflict != 0 && conflict != i)
                printf("Multiple conflicts?!\n");
             conflict = i;
          }
        }
    }

    printf("GB 0 conflicts with %lu, doing subconflict now, expecting conflict on the first 2MB:\n", conflict);
    for(size_t i = 0; i < 3; i++) {
       uint64_t a, b, c, d;

       for(size_t k = 0; k < 3; k++) {
          rdtscll(a);
          memset(&data[conflict * GB + i*LARGE_PAGE_SIZE], k, LARGE_PAGE_SIZE);
          clflush(&data[conflict * GB + i*LARGE_PAGE_SIZE], LARGE_PAGE_SIZE);
          rdtscll(b);

          rdtscll(c);
          memset(&data[0], k, LARGE_PAGE_SIZE);
          clflush(&data[0], LARGE_PAGE_SIZE);
          rdtscll(d);
          printf("2MB[0] %lu %p 2MB[%lu]\t%lu\t%p\n", d-c, &data[0], i, b-a, &data[conflict*GB+i*LARGE_PAGE_SIZE]);
        }
    }

    /*
     * To get the cache size:
     *   - First line of log_memory_1GB_0 -> pfn xxx -> 0x(xxx) * 4 * 1024 == phys addr 1 (/!\ pfn is in hex!!)
     *   - First line of log_memory_1GB_conflict -> pfn xxx -> 0x(xxx) * 4 * 1024 == phys addr 2
     *   - Size is the maximum size for which
     *         phys1 % SIZE*GB == phys2 % SIZE*GB
     *     ... because we know from the next experiments that the conflict happens
     *     on the first 2MB page of the 1GB huge pages, so the cache size is a multiple of GB.
     *
     * E.g.:
     *    pfn 780000 & pfn 1380000
     *    for i in range(1, 200):
     *       print(i, 32212254720 % (i*1024*1024*1024) == 83751862272 % (i*1024*1024*1024)) -> 48 -> cache is 48GB (per node, so 75% of memory capacity)
     */
    char *cmd;
    asprintf(&cmd, "sudo ../tmp/pagemap/pagemap %ld %p %p > log_memory_1GB_0", (long) getpid(), data, &data[LARGE_PAGE_SIZE]);
    system(cmd);
    asprintf(&cmd, "sudo ../tmp/pagemap/pagemap %ld %p %p > log_memory_1GB_conflict", (long) getpid(), &data[conflict*GB], &data[conflict*GB+LARGE_PAGE_SIZE]);
    system(cmd);


    printf("Doing subconflict 64B:\n");
    nb_segments = LARGE_PAGE_SIZE/TRASHING_CACHE_LINE_SIZE;
    for(size_t i = 0; i < 10; i++) {
       uint64_t a, b, c, d;

       for(size_t k = 0; k < 3; k++) {
          rdtscll(c);
          for(size_t j = 0; j < 100000; j++) {
             memset(&data[0], j, TRASHING_CACHE_LINE_SIZE);
             clflush(&data[0], TRASHING_CACHE_LINE_SIZE);

             memset(&data[conflict * GB + i*TRASHING_CACHE_LINE_SIZE], j+2, TRASHING_CACHE_LINE_SIZE);
             clflush(&data[conflict * GB + i*TRASHING_CACHE_LINE_SIZE], TRASHING_CACHE_LINE_SIZE);
          }
          rdtscll(d);

          printf("\t%lu size %lu: %lu\n", i, (i+1)*TRASHING_CACHE_LINE_SIZE, d-c);
        }
    }

    /*printf("Granularity of cache:\n");
    for(size_t granularity = 64; granularity < LARGE_PAGE_SIZE; granularity*=2) {
       uint64_t a, b, c, d;

       for(size_t k = 0; k < 3; k++) {
          rdtscll(c);
          for(size_t j = 0; j < 10000LU*64/granularity; j++) {
             memset(&data[0], j, granularity);
             clflush(&data[0], granularity);

             memset(&data[0], j+2, granularity);
             clflush(&data[0], granularity);
          }
          rdtscll(d);

          rdtscll(a);
          for(size_t j = 0; j < 10000LU*64/granularity; j++) {
             memset(&data[0], j, granularity);
             clflush(&data[0], granularity);

             memset(&data[conflict * GB + LARGE_PAGE_SIZE - granularity], j+2, granularity);
             clflush(&data[conflict * GB + LARGE_PAGE_SIZE - granularity], granularity);
          }
          rdtscll(b);

          printf("\t%lu: [0+0] %lu [0+%lu] %lu\n", granularity, d-c, conflict, b-a);
        }
    }*/
    return 0;
}
