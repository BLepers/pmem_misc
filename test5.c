#define _GNU_SOURCE
#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/time.h>
#include <sys/mman.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <assert.h>

#ifdef __x86_64__
#define rdtscll(val) {                                           \
       unsigned int __a,__d;                                        \
       asm volatile("rdtsc" : "=a" (__a), "=d" (__d));              \
       (val) = ((unsigned long)__a) | (((unsigned long)__d)<<32);   \
}
#else
#define rdtscll(val) __asm__ __volatile__("rdtsc" : "=A" (val))
#endif

#define TRASHING_CACHE_LINE_SIZE 64
#define GB ((size_t)1024*1024*1024)
#define HUGE_PAGE_SIZE (2*1024*1024)
#define SIZE (50LU*GB)

static inline void mfence() {
    asm volatile("sfence":::"memory");
}

static inline void clflush(char *data, size_t len) {
   volatile char *ptr = (char *)((unsigned long)data &~(TRASHING_CACHE_LINE_SIZE-1));
   assert(ptr == data);
   mfence();
   for(; ptr<&data[len]; ptr+=TRASHING_CACHE_LINE_SIZE){
      asm volatile("clflush %0" : "+m" (*(volatile char *)ptr));
   }
   mfence();
}

int main(int argc, char** argv) {
    char *data, *page;

    printf("pid: %d\n", getpid());

    posix_memalign((void**)&data, HUGE_PAGE_SIZE, SIZE);
    posix_memalign((void**)&page, HUGE_PAGE_SIZE, HUGE_PAGE_SIZE);

    if(madvise(data, SIZE, MADV_HUGEPAGE))
       printf("Error!\n");
    if(madvise(page, HUGE_PAGE_SIZE, MADV_HUGEPAGE))
       printf("Error!\n");

    printf("Memset %p\n", data);
    memset(data, 0, SIZE);
    printf("Memset %p\n", page);
    memset(page, 0, SIZE);

    char *cmd;
    asprintf(&cmd, "sudo ../tmp/pagemap/pagemap %ld %p %p > log_memory", (long) getpid(), data, &data[SIZE]);
    system(cmd);

    size_t nb_segments = SIZE/HUGE_PAGE_SIZE;
    memset(data, rand(), HUGE_PAGE_SIZE);


    for(size_t i = 0; i < nb_segments; i++) {
       uint64_t a, b, c, d;

       for(size_t k = 0; k < 3; k++) {
          rdtscll(a);
          memcpy(page, &data[i*HUGE_PAGE_SIZE], HUGE_PAGE_SIZE);
          clflush(&data[i*HUGE_PAGE_SIZE], HUGE_PAGE_SIZE);
          rdtscll(b);

          rdtscll(c);
          memcpy(page, &data[0], HUGE_PAGE_SIZE);
          clflush(&data[0], HUGE_PAGE_SIZE);
          rdtscll(d);
          printf("GB[0] %lu %p GB[%lu]\t%lu\t%p\n", d-c, &data[0], i, b-a, &data[i*HUGE_PAGE_SIZE]);
        }
    }
    return 0;
}
