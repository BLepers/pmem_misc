/*
 *  hello-1.c - The simplest kernel module.
 */
#include <linux/module.h>	/* Needed by all modules */
#include <linux/kernel.h>	/* Needed for KERN_INFO */
#include <linux/mm.h>
#include <linux/pgtable.h>

int pmd_huge(pmd_t pmd)
{
	return !pmd_none(pmd) &&
		(pmd_val(pmd) & (_PAGE_PRESENT|_PAGE_PSE)) != _PAGE_PRESENT;
}

static unsigned long vaddr2pfn(unsigned long vaddr)
{
    pgd_t *pgd;
    p4d_t *p4d;
    pud_t *pud;
    pmd_t *pmd;
    pte_t *pte;

    pgd = pgd_offset(current->mm, vaddr);
    p4d = p4d_offset(pgd, vaddr);
    pud = pud_offset(p4d, vaddr);

    pmd = pmd_offset(pud, vaddr);
    if(pmd_huge(*pmd) || pmd_large(*pmd))
       return pmd_pfn(*pmd);

    pte = pte_offset_kernel(pmd, vaddr);
    return pte_pfn(*pte);
}

void set_merge_hook(void (*_merge_hook)(pmd_t *pmd, pmd_t, unsigned long haddr));

#define SIZE 10
pmd_t* previous_pmds[SIZE];
uint64_t used_pmds = 0;
void *old_mm = NULL;

void hook_pmd(pmd_t *pmd, pmd_t _pmd, unsigned long haddr) {
   size_t i;
   pmd_t *tmp = pmd;

   printk("PMD %lx (haddr 0x%lx pfn 0x%lx): val %lu huge %d large %d present %d none %d write %d pfn %lu flags *%lu\n",
         (long unsigned)pmd,
         haddr, vaddr2pfn(haddr),
         pmd_val(*pmd),
         pmd_huge(*pmd),
         pmd_large(*pmd),
         pmd_present(*pmd),
         pmd_none(*pmd),
         pmd_write(*pmd),
         pmd_pfn(*pmd),
         pmd_flags(*pmd)
         );

   if(current->mm != old_mm) {
      old_mm = current->mm;
      used_pmds = 0;
   }
   for(i = 0; i < used_pmds; i++) {
      pmd = previous_pmds[i];
      printk("\t[%lu] PMD %lx: val %lu huge %d large %d present %d none %d write %d pfn %lu flags *%lu\n", i,
            (long unsigned)pmd,
            pmd_val(*pmd),
            pmd_huge(*pmd),
            pmd_large(*pmd),
            pmd_present(*pmd),
            pmd_none(*pmd),
            pmd_write(*pmd),
            pmd_pfn(*pmd),
            pmd_flags(*pmd)
            );
   }
   if(used_pmds < SIZE) {
      previous_pmds[used_pmds] = tmp;
      used_pmds++;
   }
}

int init_module(void)
{
   pmd_t _pmd;
   pte_t _pte;

   pgprot_t a = { _PAGE_PRESENT | _PAGE_USER | _PAGE_RW };

   _pmd = pfn_pmd(0x4ffea00, a);
   printk("Huge %d\n", pmd_huge(_pmd));
   printk("large %d\n", pmd_large(_pmd));
   printk("devmap %d\n", pmd_devmap(_pmd));
   printk("trans_huge %d\n", pmd_trans_huge(_pmd));
   printk("Present %d\n", pmd_present(_pmd));
   printk("None %d\n", pmd_none(_pmd));
   printk("protnone %d\n", pmd_protnone(_pmd));
   printk("dirty %d\n", pmd_dirty(_pmd));
   printk("young %d\n", pmd_young(_pmd));
   printk("write %d\n", pmd_write(_pmd));
   printk("pfn %lx\n", pmd_pfn(_pmd));

   _pmd = pmd_mkhuge(_pmd);
   printk("Huge %d\n", pmd_huge(_pmd));
   printk("large %d\n", pmd_large(_pmd));
   printk("devmap %d\n", pmd_devmap(_pmd));
   printk("trans_huge %d\n", pmd_trans_huge(_pmd));
   printk("Present %d\n", pmd_present(_pmd));
   printk("None %d\n", pmd_none(_pmd));
   printk("protnone %d\n", pmd_protnone(_pmd));
   printk("dirty %d\n", pmd_dirty(_pmd));
   printk("young %d\n", pmd_young(_pmd));
   printk("write %d\n", pmd_write(_pmd));
   printk("pfn %lx\n", pmd_pfn(_pmd));

   _pte = pfn_pte(0x4ffea66, a);
   printk("devmap %d\n", pte_devmap(_pte));
   printk("Present %d\n", pte_present(_pte));
   printk("None %d\n", pte_none(_pte));
   printk("protnone %d\n", pte_protnone(_pte));
   printk("dirty %d\n", pte_dirty(_pte));
   printk("young %d\n", pte_young(_pte));
   printk("write %d\n", pte_write(_pte));
   printk("pfn %lx\n", pte_pfn(_pte));

	printk(KERN_INFO "Hello world 1.\n");
   set_merge_hook(hook_pmd);
	return 0;
}

void cleanup_module(void)
{
	printk(KERN_INFO "Goodbye world 1.\n");
   set_merge_hook(NULL);
}

MODULE_LICENSE("GPL");
