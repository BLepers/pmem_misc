/*
 *  hello-1.c - The simplest kernel module.
 */
#include <linux/module.h>	/* Needed by all modules */
#include <linux/moduleparam.h>
#include <linux/kernel.h>	/* Needed for KERN_INFO */
#include <linux/mm.h>
//#include <linux/pgtable.h>
#include <linux/hugetlb.h>
#include <linux/perf_event.h>
#include <asm-generic/io.h>
#include <linux/timer.h>

#define HUGE_PAGE_SIZE (1LU*1024*1024*1024) // 1GB
#define CACHE_SIZE 48 // DRAM is 64GB -> cache is 48GB on our machine
#define pfn_to_gigantic_pfn(pfn) ((pfn)/(512*512)) // 512*512 4KB pages in a 1GB page
#define pfn_to_bucket(pfn) (pfn_to_gigantic_pfn(pfn) % CACHE_SIZE)


static int max_conflicts = 0;
static u64 allocated_pages[CACHE_SIZE];


#define PEBS_SAMPLE_TYPE PERF_SAMPLE_IP | PERF_SAMPLE_TID | PERF_SAMPLE_ADDR
#define PMEM_READ 0x80d1
#define DRAM_READ 0x20d1
#define STORE_ALL 0x82d0

static int nb_samples = 0, nb_dram_loads = 0, nb_pmem_loads = 0, nb_stores = 0;
static struct perf_event **events;
static size_t configs[] = { DRAM_READ, PMEM_READ, STORE_ALL };

#define HEAT_PRINT_SIZE 2048
static char *heatprint;
static u64 heatmap[CACHE_SIZE];
static u64 heatmap_snap[CACHE_SIZE];

#define MOVING_AVG_N 10
static struct timer_list my_timer;


static u64 predict_conflicts(struct page *page) {
   if(!allocated_pages[pfn_to_bucket(page_to_pfn(page))])
      return 0;
   if(!heatmap_snap[pfn_to_bucket(page_to_pfn(page))])
      return allocated_pages[pfn_to_bucket(page_to_pfn(page))];
   return heatmap_snap[pfn_to_bucket(page_to_pfn(page))];
   //return allocated_pages[pfn_to_bucket(page_to_pfn(page))];
}

static void reserve_page(struct hstate *h, int nid, struct page *page)
{
   size_t bucket = pfn_to_bucket(page_to_pfn(page));
   allocated_pages[bucket]++;

   list_move(&page->lru, &h->hugepage_activelist);
   set_page_count(page, 1);
   ClearHPageFreed(page);
   h->free_huge_pages--;
   h->free_huge_pages_node[nid]--;

   printk("Allocating page %lu in bucket %lu on node %d, expected conflicts %llu\n", pfn_to_gigantic_pfn(page_to_pfn(page)), bucket, nid, predict_conflicts(page));
}

static struct page *minimize_conflicts(struct hstate *h, int nid)
{
   u64 conflicts, min_conflicts = 100000;
   struct page *page, *best_page = NULL;
   bool pin = !!(current->flags & PF_MEMALLOC_PIN);

   lockdep_assert_held(&hugetlb_lock);

   list_for_each_entry(page, &h->hugepage_freelists[nid], lru) {
      if (pin && !is_pinnable_page(page))
         continue;

      if (PageHWPoison(page))
         continue;

      conflicts = predict_conflicts(page);
      if(conflicts < min_conflicts) {
         min_conflicts = conflicts;
         best_page = page;
      }
   }

   if(best_page)
      reserve_page(h, nid, best_page);
   return best_page;
}

static struct page *maximize_conflicts(struct hstate *h, int nid)
{
   u64 conflicts, max_conflicts = 0;
   struct page *page, *best_page = NULL;
   bool pin = !!(current->flags & PF_MEMALLOC_PIN);

   lockdep_assert_held(&hugetlb_lock);

   list_for_each_entry(page, &h->hugepage_freelists[nid], lru) {
      if (pin && !is_pinnable_page(page))
         continue;

      if (PageHWPoison(page))
         continue;

      conflicts = predict_conflicts(page);
      if(conflicts > max_conflicts || max_conflicts == 0) {
         max_conflicts = conflicts;
         best_page = page;
      }
   }

   if(best_page)
      reserve_page(h, nid, best_page);
   return best_page;
}

static u64 perf_virt_to_phys(u64 virt)
{
	u64 phys_addr = 0;

	if (!virt)
		return 0;

	if (virt >= TASK_SIZE) {
		if (virt_addr_valid((void *)(uintptr_t)virt) &&
		    !(virt >= VMALLOC_START && virt < VMALLOC_END))
			phys_addr = (u64)virt_to_phys((void *)(uintptr_t)virt);
	} else {
		if (current->mm != NULL) {
			struct page *p;
			pagefault_disable();
			if (get_user_page_fast_only(virt, 0, &p)) {
				phys_addr = (page_to_pfn(p) << PAGE_SHIFT) + virt % PAGE_SIZE;
				put_page(p);
			}
			pagefault_enable();
		}
	}
	return phys_addr;
}

static void pebs_sample(struct perf_event *event, struct perf_sample_data *data, struct pt_regs *regs)
{
   size_t phys = perf_virt_to_phys(data->addr);
   size_t bucket = (phys / HUGE_PAGE_SIZE) % CACHE_SIZE;
   heatmap[bucket]++;
   switch(event->attr.config) {
      case DRAM_READ:
         nb_dram_loads++;
         break;
      case PMEM_READ:
         nb_pmem_loads++;
         break;
      case STORE_ALL:
         nb_stores++;
         break;
   }
   nb_samples++;
   //printk("Event %p (config %llx) CPU %u vs %u Tid %u Virt addr: %llx Phys %llx\n", event, event->attr.config, data->cpu_entry.cpu, smp_processor_id(), current->pid, data->addr, perf_virt_to_phys(data->addr));
}

static void my_timer_callback(struct timer_list *timer) {
   size_t i, print_idx = 0;

   printk("Samples: %d DRAM %d PMEM %d STORE\n", nb_dram_loads, nb_pmem_loads, nb_stores);
   print_idx += snprintf(&heatprint[print_idx], HEAT_PRINT_SIZE - print_idx, "Heatmap: ");
   for(i = 0; i < CACHE_SIZE; i++)
      print_idx += snprintf(&heatprint[print_idx], HEAT_PRINT_SIZE - print_idx, "%5llu ", heatmap_snap[i]);
   printk(KERN_INFO "%s", heatprint);

   for(i = 0; i < CACHE_SIZE; i++) {
      u64 new = heatmap[i];
      u64 old = heatmap_snap[i];
      u64 moving_avg = old*(MOVING_AVG_N - 1)/(MOVING_AVG_N) + new;
      heatmap_snap[i] = moving_avg;
      heatmap[i] = 0;
   }
   nb_dram_loads = nb_pmem_loads = nb_stores = 0;
   mod_timer(&my_timer, jiffies + msecs_to_jiffies(1000));
}

int init_module(void)
{
   size_t config, cpu, ncpus = num_online_cpus();
   static struct perf_event_attr wd_hw_attr = {
      .type = PERF_TYPE_RAW,
      .size		= sizeof(struct perf_event_attr),
      .pinned		= 0,
      .disabled	= 1,
      .precise_ip = 2,
      .sample_id_all = 1,
      .exclude_kernel = 1,
      .exclude_guest = 1,
      .exclude_hv = 0,
      .exclude_user =0,
      .sample_type = PERF_SAMPLE_IP | PERF_SAMPLE_TID | PERF_SAMPLE_WEIGHT | PERF_SAMPLE_ADDR | PERF_SAMPLE_PHYS_ADDR,
   };

   events = vmalloc(ncpus * ARRAY_SIZE(configs) * sizeof(*events));
   printk(KERN_INFO "Creating %lu events - %lu configs %lu cpus\n", ARRAY_SIZE(configs) * ncpus, ARRAY_SIZE(configs), ncpus);
   for(config = 0; config < ARRAY_SIZE(configs); config++) {
      for(cpu = 0; cpu < ncpus; cpu++) {
         size_t idx = config * ncpus + cpu;
         wd_hw_attr.config = configs[config];
         if(configs[config] == STORE_ALL)
            wd_hw_attr.sample_period = 10000000;
         else
            wd_hw_attr.sample_period = 100000;
         events[idx] = perf_event_create_kernel_counter(&wd_hw_attr, cpu, NULL, pebs_sample, NULL);
         if(IS_ERR(events[idx])) {
            printk(KERN_INFO "Could not create event %lu on cpu %lu\n", configs[config], cpu);
            return -1;
         }
         perf_event_enable(events[idx]);
      }
   }

   if(max_conflicts)
      set_dequeue_hook(maximize_conflicts);
   else
      set_dequeue_hook(minimize_conflicts);

   heatprint = vmalloc(HEAT_PRINT_SIZE);
   memset(heatprint, 0, HEAT_PRINT_SIZE);
   timer_setup(&my_timer, my_timer_callback, 0);
   mod_timer(&my_timer, jiffies + msecs_to_jiffies(1000));
	return 0;
}

void cleanup_module(void)
{
   size_t cpu, config, ncpus = num_online_cpus();
   set_dequeue_hook(NULL);
   del_timer(&my_timer);
	printk(KERN_INFO "Goodbye world nb_samples %d.\n", nb_samples);
   for(config = 0; config < ARRAY_SIZE(configs); config++) {
      for(cpu = 0; cpu < ncpus; cpu++) {
         size_t idx = config * ncpus + cpu;
         perf_event_disable(events[idx]);
         perf_event_release_kernel(events[idx]);
      }
   }
   vfree(heatprint);
}

module_param(max_conflicts, int, S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH);
MODULE_PARM_DESC(max_conflicts, "Set to 1 to maximize conflicts");
MODULE_LICENSE("GPL");
