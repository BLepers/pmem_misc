#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <sys/time.h>

#define GB (1LU*1024*1024*1024)
#define SIZE (100*GB)

#define declare_timer uint64_t elapsed; \
   struct timeval st, et;

#define start_timer gettimeofday(&st,NULL);

#define stop_timer(msg, args...) ;do { \
   gettimeofday(&et,NULL); \
   elapsed = ((et.tv_sec - st.tv_sec) * 1000000) + (et.tv_usec - st.tv_usec) + 1; \
   printf("(%s,%d) [%6lums] " msg "\n", __FUNCTION__ , __LINE__, elapsed/1000, ##args); \
} while(0)

int main() {
   declare_timer;

   char *t = malloc(SIZE);

   printf("Init:\n");
   for(size_t i = 0; i < SIZE - GB; i += GB) {
      start_timer {
         memset(&t[i], 0, GB);
      } stop_timer("[%lu]", i/GB);
   }

   for(size_t j = 45*GB; j < 50*GB; j+= GB) {
      for(size_t k = 0; k < 3; k++) {
         printf("%lu GB, %lu iteration:\n", j, k);
         for(size_t i = 0; i < j; i += GB) {
            start_timer {
               memset(&t[i], rand(), GB);
            } stop_timer("[%lu]", i/GB);
         }
      }
   }

   for(size_t k = 0; k < 3; k++) {
      printf("WAYS:\n");
      for(size_t i = 0; i < 5*GB; i += GB) {
         start_timer {
            memset(&t[i], rand(), GB);
         } stop_timer("[%lu]", i/GB);
      }
      for(size_t i = 44*GB; i < 50*GB; i += GB) {
         start_timer {
            memset(&t[i], rand(), GB);
         } stop_timer("[%lu]", i/GB);
      }
   }
}

