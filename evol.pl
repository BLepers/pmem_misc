#!/usr/bin/perl
use strict;
use warnings;

my @files = @ARGV;
my %res;

for my $f (@files) {
   open(F, $f);
   my @lines = <F>;
   close(F);

   my ($xp)= ($f =~ /log\.(.*?)\./);
   my ($s, $p, $v) = ($lines[0] =~ /Size (\d+) Percent (\d+) Value (\d+)/);

   for my $l (@lines) {
      if($l =~ /Cycles (\d+)/) {
         $res{$xp}->{"$s.$p.$v"} = $1;
      } 
   }
}

for my $f (sort keys %{$res{"linux"}}) {
   $res{"static"}->{$f} //= 1;
   $res{"hemem"}->{$f} //= 1;
   $res{"dram"}->{$f} //= 1;
   print "$f - ".$res{"linux"}->{$f}." - ".$res{"static"}->{$f}." -- ".($res{"linux"}->{$f}/$res{"static"}->{$f}*100-100)."\n";
   #print "\t - ".$res{"reads"}->{$f}." - ".$res{"staticreads"}->{$f}." -- ".($res{"reads"}->{$f}/$res{"staticreads"}->{$f}*100-100)."\n";
   print "\t - ".$res{"dram"}->{$f}." -- ".($res{"static"}->{$f}/$res{"dram"}->{$f}*100-100)."\n";
   print "\t - ".$res{"hemem"}->{$f}." -- ".($res{"static"}->{$f}/$res{"hemem"}->{$f}*100-100)."\n";
}
