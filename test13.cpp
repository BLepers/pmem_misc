#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/time.h>
#include <sys/mman.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <assert.h>
#include <linux/mman.h>
#include <iostream>
#include <chrono>
#include <omp.h>
#define TBB_SUPPRESS_DEPRECATED_MESSAGES 1
#include "tbb/tbb.h"

#ifdef __x86_64__
#define rdtscll(val) {                                           \
       unsigned int __a,__d;                                        \
       asm volatile("rdtsc" : "=a" (__a), "=d" (__d));              \
       (val) = ((unsigned long)__a) | (((unsigned long)__d)<<32);   \
}
#else
#define rdtscll(val) __asm__ __volatile__("rdtsc" : "=A" (val))
#endif

#define TRASHING_CACHE_LINE_SIZE 64
#define GB ((size_t)1024*1024*1024)
#define LARGE_PAGE_SIZE (2LU*1024*1024)


static __uint128_t __thread g_lehmer64_state;
void init_seed(void) {
   g_lehmer64_state = rand();
}
uint64_t lehmer64() {
  g_lehmer64_state *= 0xda942042e4dd58b5;
  return g_lehmer64_state >> 64;
}

int main(int argc, char** argv) {
   size_t size = GB*atoi(argv[1]);
   size_t percent_hot = atoi(argv[2]);
   size_t value_size = atoi(argv[3]);
   size_t early_stop = atoi(argv[4]);

   tbb::task_scheduler_init init(10);

   printf("Size %lu Percent %lu Value %lu\n", size, percent_hot, value_size);
   char *data;
   data = (char*)mmap(NULL, size, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS | MAP_HUGETLB, 0, 0);
   if(data == MAP_FAILED)
      printf("Cannot allocate gigantic pages, do echo 60 > /sys/devices/system/node/node0/hugepages/hugepages-1048576kB/nr_hugepages\n");
   //data = (char*)mmap(NULL, size, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS, 0, 0);
   //printf("WARNING, NOT USING LARGE PAGES, ONLY FOR HEMEM!\n");

   {
      auto starttime = std::chrono::system_clock::now();
      tbb::parallel_for(tbb::blocked_range<uint64_t>(0, size/4096), [&](const tbb::blocked_range<uint64_t> &range) {
            char *tmp = (char*)malloc(4096);
            for (uint64_t i = range.begin(); i != range.end(); i++) {
               memcpy(&data[i*4096], tmp, 4096);
            }
            free(tmp);
      });
      auto duration = std::chrono::duration_cast<std::chrono::microseconds>(
            std::chrono::system_clock::now() - starttime);
      printf("Init done %fs\n", duration.count() / 1000000.0);
   }
   //memset(data, 0, size);

   {
      auto starttime = std::chrono::system_clock::now();
      tbb::parallel_for(tbb::blocked_range<uint64_t>(0, size/4096), [&](const tbb::blocked_range<uint64_t> &range) {
            char *tmp = (char*)malloc(4096);
            for (uint64_t i = range.begin(); i != range.end(); i++) {
               memcpy(tmp, &data[i*4096], 4096);
            }
            free(tmp);
      });
      auto duration = std::chrono::duration_cast<std::chrono::microseconds>(
            std::chrono::system_clock::now() - starttime);
      printf("Clearing %fs\n", duration.count() / 1000000.0);
   }

   if(early_stop)
      exit(0);

   sleep(5);

   size_t nb_indexes = size/value_size;
   size_t nb_hot = nb_indexes * percent_hot / 100;
   printf("Init %lu randoms\n", nb_hot);
   int *indexes = (int*)malloc(nb_indexes*sizeof(*indexes));
   init_seed();
   for(size_t i = 0; i < nb_indexes; i++)
      indexes[i] = (nb_indexes/4+i)%nb_indexes;
   /*for(size_t i = 0; i < nb_indexes; i++) {
      size_t tmp = lehmer64() % nb_indexes;
      size_t old = indexes[tmp];
      indexes[tmp] = indexes[i];
      indexes[i] = old;
   }*/

   //tbb::parallel_for(tbb::blocked_range<uint64_t>(0, NB_REQS/NB_REQ_PER_ITERATION), [&](const tbb::blocked_range<uint64_t> &range) {
   //system("sudo env -i /home/blepers/linux-huge/tools/perf/perf stat -a -e mem_load_l3_miss_retired.remote_pmm,mem_load_retired.local_pmm,mem_load_l3_miss_retired.local_dram sleep 1000 &");
   //sleep(1);
   omp_set_num_threads(10);
#pragma omp parallel
   {
      void *tmp = malloc(value_size);
      //size_t nb_it = 100;
      size_t nb_it = 300;
      size_t nb_req_per_it = 1024LU*1024*1024/value_size;
      size_t show = omp_get_thread_num() == 0;
      init_seed();
      for(size_t i = 0; i < nb_it; i++) {
         uint64_t a, b;
         rdtscll(a);
         for(size_t j = 0; j < nb_req_per_it; j++) {
            //memcpy(tmp, &data[indexes[lehmer64()%nb_hot]], value_size);
            memcpy(&data[indexes[lehmer64()%nb_hot]*value_size], tmp, value_size);
         }
         rdtscll(b);
         if(show)
            printf("Cycles %lu\n", b -a);
         //system("sudo env -i killall -w sleep");
      }
      free(tmp);
      //});
   }
      //system("sudo env -i killall -w sleep");
   return 0;
}
