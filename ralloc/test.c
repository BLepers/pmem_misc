#define _GNU_SOURCE
#include <stdio.h>
#include "ralloc.h"

int main() {
   printf("8 %p\n", RP_malloc(8));
   printf("8 %p\n", RP_malloc(8));
   printf("9 %p\n", RP_malloc(9));
   printf("16 %p\n", RP_malloc(16));
   printf("32 %p\n", RP_malloc(32));
   printf("64 %p\n", RP_malloc(64));
   printf("128 %p\n", RP_malloc(128));
   printf("256 %p\n", RP_malloc(256));
   printf("512 %p\n", RP_malloc(512));
   for(size_t i = 0; i < (2*1024*1024)/512; i++)
      printf("512 %p\n", RP_malloc(512));
   return 0;
}
