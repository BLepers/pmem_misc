#ifndef RALLOC_H

#ifdef __cplusplus
extern "C" {
#endif

void *RP_malloc(size_t size);
void RP_free(void *a);

#ifdef __cplusplus
}
#endif

#endif
