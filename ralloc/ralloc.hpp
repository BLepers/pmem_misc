#ifndef RALLOC_H
#define RALLOC_H

#ifdef __cplusplus
extern "C" {
#endif

int RP_init(const char* _id, size_t size);
void *RP_malloc(size_t size);
void *RP_calloc(size_t num, size_t size);
void *RP_realloc(void *a, size_t size);
void RP_free(void *a);
void RP_close(void);
void RP_recover(void);
void* RP_set_root(void* ptr, size_t i);

#ifdef __cplusplus
}
#endif

template<class T>
T* RP_get_root(size_t i){
    return NULL;
}

#endif
