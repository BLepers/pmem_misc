#define _GNU_SOURCE
#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/time.h>
#include <sys/mman.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <assert.h>
#include "ralloc.h"

#ifdef __x86_64__
#define rdtscll(val) {                                           \
       unsigned int __a,__d;                                        \
       asm volatile("rdtsc" : "=a" (__a), "=d" (__d));              \
       (val) = ((unsigned long)__a) | (((unsigned long)__d)<<32);   \
}
/*#define rdtscll(val) val = 0*/
#else
#define rdtscll(val) __asm__ __volatile__("rdtsc" : "=A" (val))
#endif

#define USE_PROFILING 0
#define PROFILING_SAMPLES 5000
#define PROFILING_PRINT_LIMIT 100000LU

#if USE_PROFILING

/*#define declare_timer uint64_t ___a,___b;
#define start_timer rdtscll(___a);
#define stop_timer do {                                                                   \
   rdtscll(___b);                                                                         \
   if(!thread_cache.latencies)                                                            \
      thread_cache.latencies = malloc(PROFILING_SAMPLES*sizeof(thread_cache.latencies));  \
   if(thread_cache.nb_latencies < PROFILING_SAMPLES) {                                    \
      thread_cache.latencies[thread_cache.nb_latencies] = ___b - ___a;                    \
      thread_cache.nb_latencies++;                                                        \
   } else if(thread_cache.nb_latencies == PROFILING_SAMPLES) {                            \
      thread_cache.nb_latencies++;                                                        \
      for(size_t ___i = 1; ___i < PROFILING_SAMPLES; ___i++)                              \
         printf("Lat: %lu\n", thread_cache.latencies[___i]);                              \
   }                                                                                      \
} while(0);*/

#define declare_timer uint64_t ___a,___b;
#define start_timer rdtscll(___a);
#define stop_timer do {                                                                   \
   rdtscll(___b);                                                                         \
   thread_cache.nb_latencies++;                                                           \
   if(thread_cache.thread_id == 1 && ___b - ___a > PROFILING_PRINT_LIMIT)                                                \
      printf("%lu %lu\n", thread_cache.nb_latencies, ___b - ___a);                   \
} while(0);

#else

#define declare_timer
#define start_timer
#define stop_timer

#endif

#define USE_MADVISE 0
#define USE_MERGE_MADVISE 0
//#define PATH "/pmem0/file-t%lu-f%lu"
#define PATH "/pmem0/file-t%lu-f%lu"
//#define PATH "/pmem5/file-t%lu-f%lu"

/* Various convenient sizes */
#define CACHE_LINE_SIZE 64LU
#define PMEM_GRANULARITY (256LU)
#define PAGE_SIZE (4LU*1024)
#define HUGE_PAGE_SIZE (2LU*1024*1024)
#define FILE_SIZE (100LU*HUGE_PAGE_SIZE)
#define NB_SUPERBLOCKS_PER_FILE (FILE_SIZE/HUGE_PAGE_SIZE - 1)

/*
 * Size of elements in the superblocks.
 * For simplicity we do powers of 2, i.e., a block only contains values of size 2**class
 */
#define LOG2(X) ((unsigned) (8*sizeof (unsigned long long) - __builtin_clzll((X)) - 1))
#define NB_CLASSES 20LU

#define die(msg, args...) do { \
   printf("(%s,%d) " msg "\n", __FUNCTION__ , __LINE__, ##args); \
   fflush(stdout); \
   exit(-1); \
} while(0)

//#define debug(args...) printf(args)
#define debug(args...)

/* Superblock */
struct superblock_meta {
   uint64_t owner;
   uint64_t data_size;
   uint64_t last_alloc;
   uint64_t *free_list;
   uint64_t *free_list_atomic;
   uint64_t *next_block;
} __attribute__((packed));

struct superblock {
   struct superblock_meta meta;
   char padding[PMEM_GRANULARITY - sizeof(struct superblock_meta)];
   char data[HUGE_PAGE_SIZE - PMEM_GRANULARITY];
} __attribute__((packed));

/* File */
struct file_meta {
   uint64_t last_avail_block;
} __attribute__((packed));

struct file {
   struct file_meta meta;
   char padding[HUGE_PAGE_SIZE - sizeof(struct file_meta)];
   struct superblock blocks[NB_SUPERBLOCKS_PER_FILE];
} __attribute__((packed));

/* Per thread cache */
struct thread_cache {
   size_t thread_id;
   size_t nb_files;
   size_t nb_latencies;
   size_t last_read;
   //size_t *latencies;
   struct file *file;
   struct superblock *blocks[NB_CLASSES];
};
static size_t nb_threads;
static __thread struct thread_cache thread_cache;

/*
 * Create a new file of FILE_SIZE and madvise it
 */
static void create_new_file() {
   char *path;
   if(asprintf(&path, PATH, thread_cache.thread_id, thread_cache.nb_files++)) {};

   int fd = open(path,  O_RDWR | O_CREAT | O_DIRECT | O_TRUNC, 0777);
   if(fd == -1)
      die("Cannot open %s\n", path);

   posix_fallocate(fd, 0, FILE_SIZE);
   thread_cache.file = mmap(NULL, FILE_SIZE, PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0);
   if(USE_MADVISE && madvise(thread_cache.file, FILE_SIZE, MADV_NOHUGEPAGE))
      die("Error madvising!\n");

   thread_cache.file->meta.last_avail_block = 0;


   close(fd);
   free(path);


   /*static int fd;
   if(!fd)
      fd = open(path,  O_RDWR | O_CREAT | O_DIRECT | O_TRUNC, 0777);
   if(fd == -1)
      die("Cannot open %s\n", path);
   posix_fallocate(fd, 0, thread_cache.nb_files*FILE_SIZE);
   thread_cache.file = mmap(NULL, FILE_SIZE, PROT_READ | PROT_WRITE, MAP_SHARED, fd, ((thread_cache.nb_files-1)*FILE_SIZE));
   if(USE_MADVISE && madvise(thread_cache.file, FILE_SIZE, MADV_NOHUGEPAGE))
      die("Error madvising!\n");
   thread_cache.file->meta.last_avail_block = 0;
   free(path);*/

   debug("Created file %s size %luB\n", path, FILE_SIZE);
   return;
}

/*
 * Allocate a superblock from a file, create a new file if the old file is full
 */
static struct superblock *get_new_block(size_t class) {
   if(!thread_cache.file || thread_cache.file->meta.last_avail_block == NB_SUPERBLOCKS_PER_FILE)
      create_new_file();

   size_t n = thread_cache.file->meta.last_avail_block;
   struct superblock *b = &thread_cache.file->blocks[n];
   b->meta.owner = thread_cache.thread_id;
   b->meta.data_size = 1 << class;
   b->meta.last_alloc = 0;
   b->meta.free_list = NULL;
   b->meta.free_list_atomic = NULL;
   b->meta.next_block = NULL;

   thread_cache.blocks[class] = b;
   thread_cache.file->meta.last_avail_block++;

   debug("Created a block of size %lu\n", b->meta.data_size);
   return b;
}

/*
 * Allocate an element from a superblock and madvise the superblock
 * once all elements have been allocated
 */
static void *get_slot(struct superblock *b) {
   void *ret;
   if(b->meta.free_list) {
      ret = b->meta.free_list;
      b->meta.free_list = *(void**)(b->meta.free_list);
      return ret;
   }
   if(b->meta.free_list_atomic) {
      b->meta.free_list = b->meta.free_list_atomic;
      b->meta.free_list_atomic = NULL; // TODO, CaS
      ret = b->meta.free_list;
      b->meta.free_list = *(void**)(b->meta.free_list);
      return ret;
   }

   if(b->meta.last_alloc + b->meta.data_size >= sizeof(b->data)) {
      if(USE_MADVISE && USE_MERGE_MADVISE)
         madvise(b, HUGE_PAGE_SIZE, MADV_HUGEPAGE);
      return NULL;
   }

   ret = &b->data[b->meta.last_alloc];
   b->meta.last_alloc += b->meta.data_size;
   return ret;
}

/*
 * Allocate a big file for large mallocs
 */
static void *get_file_for_big_malloc(size_t size) {
   size_t file_size = size + CACHE_LINE_SIZE;
   if(file_size % HUGE_PAGE_SIZE != 0)
      file_size = file_size + HUGE_PAGE_SIZE - (file_size%HUGE_PAGE_SIZE);

   char *path;
   if(asprintf(&path, PATH, thread_cache.thread_id, thread_cache.nb_files++)) {};

   int fd = open(path,  O_RDWR | O_CREAT | O_DIRECT, 0777);
   if(fd == -1)
      die("Cannot open %s\n", path);

   posix_fallocate(fd, 0, file_size);

   struct superblock *b = mmap(NULL, file_size, PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0);
   b->meta.data_size = file_size - CACHE_LINE_SIZE;

   close(fd);
   free(path);

   debug("Created file %s size %luB for a large alloc of size %luB\n", path, file_size, size);
   return b->data;
}

void *RP_malloc(size_t size) {
   void *ret;
   declare_timer;

   if(thread_cache.thread_id == 0) {
      memset(&thread_cache, 0, sizeof(thread_cache));
      thread_cache.thread_id = __sync_add_and_fetch(&nb_threads, 1);
      debug("First allocation, now our thread id is %lu\n", thread_cache.thread_id);
      if(thread_cache.thread_id == 1) {
         printf("#RALLOC Config: USE_MADVISE %d USE_MERGE_MADVISE %d\n", USE_MADVISE, USE_MERGE_MADVISE);
         fprintf(stderr, "#RALLOC Config: USE_MADVISE %d USE_MERGE_MADVISE %d\n", USE_MADVISE, USE_MERGE_MADVISE);
      }
   }

   //if(thread_cache.nb_latencies== 16381) {
      //if(system("sudo /home/blepers/linux/tools/perf/perf record -g -C 20 &")) {};
      //if(system("sudo /home/blepers/linux/tools/perf/perf record -g -a &")) {};
      //sleep(4);
      //}
   start_timer {
      size_t class = LOG2(size);
      if(class < 3)
         class = 3; // 8B minimum
      if(size > (1 << class))
         class++;
      if(class > NB_CLASSES)
         return get_file_for_big_malloc(size);
         //die("Size %lu is too big (class %lu, max is %lu)!\n", size, class, NB_CLASSES);

      struct superblock *b = thread_cache.blocks[class];
      if(!b)
         b = get_new_block(class);

      ret = get_slot(b);
      if(!ret) {
         b = get_new_block(class);
         ret = get_slot(b);
      }
   } stop_timer;
   //if(thread_cache.nb_latencies == 16382) {
   //if(system("sudo killall -w -INT perf")) {};
   //}


   //if(((size_t)ret) - thread_cache.last_read != 128)
   //printf("%lu\n", ((size_t)ret) - thread_cache.last_read);
   //thread_cache.last_read = (size_t)ret;
   *(size_t*)ret = 3;
   return ret;
}

void *RP_calloc(size_t num, size_t size) {
   void *dst = RP_malloc(num*size);
   memset(dst, 0, num*size);
   return dst;
}

void *RP_realloc(void *a, size_t size) {
   if(!a)
      return RP_malloc(size);

   struct superblock *b = (void*) ( ( ((long unsigned)a) / HUGE_PAGE_SIZE ) * HUGE_PAGE_SIZE );
   size_t block_size = b->meta.data_size;
   if(block_size >= size)
      return a;

   void *dst = RP_malloc(size);
   memcpy(dst, a, block_size);
   RP_free(a);
   return dst;
}

void RP_free(void *a) {
   struct superblock *b = (void*) ( ( ((long unsigned)a) / HUGE_PAGE_SIZE ) * HUGE_PAGE_SIZE );
   if(b->meta.owner == thread_cache.thread_id) {
      *(void**)a = b->meta.free_list;
      b->meta.free_list = a;
   } else {
      while(1) {
         *(volatile void**)a = (volatile void*)b->meta.free_list_atomic;
         if(__sync_bool_compare_and_swap(&b->meta.free_list_atomic, *(void**)a, a))
            break;
      }
   }
   return;
}

int RP_init(const char* _id, uint64_t size) {
   return 0;
}

void RP_close(void) {
   return;
}

void RP_recover(void) {
   return;
}

void* RP_set_root(void* ptr, uint64_t i) {
   return NULL;
}
