CFLAGS=-O0
TARGETS=test4 test5 test6 test7 test8 test9 test10 test11 test12 test13 test14

.PHONY:all

all: ${TARGETS}

clean:
	rm -rf *.o ${TARGETS}

test9: LDLIBS=-ltbb -fopenmp
test9: CXXFLAGS=-O0
test10: LDLIBS=-ltbb
test10: CXXFLAGS=-O0
test12: LDLIBS=-ltbb -fopenmp
test12: CXXFLAGS=-O0
test13: LDLIBS=-ltbb -fopenmp
test13: CXXFLAGS=-O0
test14: LDLIBS=-ltbb -fopenmp -lpmem
test14: CXXFLAGS=-O0
