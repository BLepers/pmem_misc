#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/time.h>
#include <sys/mman.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <assert.h>
#include <linux/mman.h>
#include <iostream>
#include <chrono>
#define TBB_SUPPRESS_DEPRECATED_MESSAGES 1
#include "tbb/tbb.h"

#ifdef __x86_64__
#define rdtscll(val) {                                           \
       unsigned int __a,__d;                                        \
       asm volatile("rdtsc" : "=a" (__a), "=d" (__d));              \
       (val) = ((unsigned long)__a) | (((unsigned long)__d)<<32);   \
}
#else
#define rdtscll(val) __asm__ __volatile__("rdtsc" : "=A" (val))
#endif

#define TRASHING_CACHE_LINE_SIZE 64
#define GB ((size_t)1024*1024*1024)
#define LARGE_PAGE_SIZE (2LU*1024*1024)


static __uint128_t __thread g_lehmer64_state;
void init_seed(void) {
   g_lehmer64_state = rand();
}
uint64_t lehmer64() {
  g_lehmer64_state *= 0xda942042e4dd58b5;
  return g_lehmer64_state >> 64;
}

int main(int argc, char** argv) {
   size_t size = GB*atoi(argv[1]);
   size_t percent_hot = atoi(argv[2]);
   size_t value_size = atoi(argv[3]);

   tbb::task_scheduler_init init(10);

   printf("Size %lu Percent %lu Value %lu\n", size, percent_hot, value_size);
   char *data;
   data = (char*)mmap(NULL, size, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS | MAP_HUGETLB, 0, 0);
   //data = (char*)mmap(NULL, size, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS, 0, 0);
   if(data == MAP_FAILED)
      printf("Cannot allocate gigantic pages, do echo 60 > /sys/devices/system/node/node0/hugepages/hugepages-1048576kB/nr_hugepages\n");

   {
      auto starttime = std::chrono::system_clock::now();
      tbb::parallel_for(tbb::blocked_range<uint64_t>(0, size/4096), [&](const tbb::blocked_range<uint64_t> &range) {
            char *tmp = (char*)malloc(4096);
            for (uint64_t i = range.begin(); i != range.end(); i++) {
               memcpy(&data[i*4096], tmp, 4096);
            }
            free(tmp);
      });
      auto duration = std::chrono::duration_cast<std::chrono::microseconds>(
            std::chrono::system_clock::now() - starttime);
      printf("Init done %fs\n", duration.count() / 1000000.0);
   }

   {
      auto starttime = std::chrono::system_clock::now();
      tbb::parallel_for(tbb::blocked_range<uint64_t>(0, size/4096), [&](const tbb::blocked_range<uint64_t> &range) {
            char *tmp = (char*)malloc(4096);
            for (uint64_t i = range.begin(); i != range.end(); i++) {
               memcpy(tmp, &data[i*4096], 4096);
            }
            free(tmp);
      });
      auto duration = std::chrono::duration_cast<std::chrono::microseconds>(
            std::chrono::system_clock::now() - starttime);
      printf("Clearing %fs\n", duration.count() / 1000000.0);
   }

#define NB_REQS 100000000
#define NB_REQ_PER_ITERATION 10000

   size_t nb_indexes = size/value_size;
   size_t nb_hot = nb_indexes * percent_hot / 100;
   printf("Init %lu randoms\n", nb_hot);
   int *indexes = (int*)malloc(nb_hot*sizeof(*indexes));
   init_seed();
   for(size_t i = 0; i < nb_hot; i++)
      indexes[i] = lehmer64() % nb_indexes;

   //tbb::parallel_for(tbb::blocked_range<uint64_t>(0, NB_REQS/NB_REQ_PER_ITERATION), [&](const tbb::blocked_range<uint64_t> &range) {
   {
      void *tmp = malloc(value_size);
      size_t nb_req_per_it = NB_REQS/NB_REQ_PER_ITERATION;
      if(value_size > 4096)
         nb_req_per_it /= 512;
      for(size_t i = 0; i < NB_REQS/NB_REQ_PER_ITERATION; i++) {
         //system("sudo env -i /home/blepers/linux-huge/tools/perf/perf stat -a -e mem_load_l3_miss_retired.remote_pmm,mem_load_retired.local_pmm,mem_load_l3_miss_retired.local_dram sleep 1000 &");
         system("sudo env -i /home/blepers/linux-huge/tools/perf/perf stat -a -e unc_m_pmm_bandwidth.read,unc_m_pmm_bandwidth.write sleep 1000 &");
         sleep(1);
         uint64_t a, b;
         rdtscll(a);
         //for(size_t j = 0; j < NB_REQ_PER_ITERATION; j++) {
         for(size_t j = 0; j < nb_req_per_it; j++) {
            memcpy(tmp, &data[indexes[lehmer64()%nb_hot]], value_size);
            //memcpy(&data[indexes[lehmer64()%nb_hot]*value_size], tmp, value_size);
         }
         rdtscll(b);
         printf("Cycles %lu\n", b -a);
         system("sudo env -i killall -w sleep");
      }
      free(tmp);
   }
   return 0;
}
