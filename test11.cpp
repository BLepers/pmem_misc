#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/time.h>
#include <sys/mman.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <assert.h>
#include <linux/mman.h>
#include <iostream>
#include <chrono>

static __uint128_t __thread g_lehmer64_state;
void init_seed(void) {
   g_lehmer64_state = rand();
}
uint64_t lehmer64() {
  g_lehmer64_state *= 0xda942042e4dd58b5;
  return g_lehmer64_state >> 64;
}

int main(int argc, char** argv) {
   const size_t cache_size = 48*1024/2; // in pages
   size_t size = atoi(argv[1]);
   assert(size % cache_size == 0); //TOFIX
   size_t percent_hot = atoi(argv[2]);
   init_seed();

   size_t *heat = (size_t*)calloc(size, sizeof(*heat));
   size_t *is_hot = (size_t*)calloc(size, sizeof(*heat));

   size_t nb_hots = size*percent_hot/100;
   size_t curr_hot_idx = 0;
   size_t *hot_idx = (size_t*)calloc(nb_hots, sizeof(*heat));

   for(size_t i = 0; i < nb_hots; i++) {
      while(1) {
         size_t idx = lehmer64()%size;
         if(!is_hot[idx]) {
            is_hot[idx] = 1;
            hot_idx[curr_hot_idx++] = idx;
            break;
         }
      }
   }

   size_t total_conflicts = 0;
   for(size_t i = 0; i < cache_size; i++) {
      //size_t total = is_hot[i] + is_hot[i+cache_size] + is_hot[i+2*cache_size];
      size_t total = is_hot[i] + is_hot[i+cache_size];
      if(total >= 2)
         total_conflicts += total -1;
   }
   fprintf(stderr, "#Conflicts to solve: %lu\n", total_conflicts);

   size_t hots = 0, conflicts = 0;
   for(size_t i = 0; i < 1000000; i++) {
      size_t loc = hot_idx[lehmer64() % nb_hots];
      size_t otherloc = (loc + cache_size) % size;
      //size_t otherloc2 = (loc + 2*cache_size) % size;
      heat[loc]++;
      if(is_hot[loc] && heat[loc] == 8)
         printf("%lu %lu\n", i, ++hots);
      if(is_hot[loc] && heat[loc] == 8 && is_hot[otherloc] && heat[otherloc] >= 8)
         fprintf(stderr, "%lu %lu\n", i, ++conflicts);
      //if(is_hot[loc] && heat[loc] == 8 && is_hot[otherloc2] && heat[otherloc2] >= 8)
      //fprintf(stderr, "%lu %lu\n", i, ++conflicts);
   }

   return 0;
}

