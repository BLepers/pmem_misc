#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <sys/time.h>

#define GB (1LU*1024*1024*1024)
//#define SIZE (100*GB)
#define SIZE (100*GB)

#define declare_timer uint64_t elapsed; \
   struct timeval st, et;

#define start_timer gettimeofday(&st,NULL);

#define stop_timer(msg, args...) ;do { \
   gettimeofday(&et,NULL); \
   elapsed = ((et.tv_sec - st.tv_sec) * 1000000) + (et.tv_usec - st.tv_usec) + 1; \
   printf("(%s,%d) [%6lums] " msg "\n", __FUNCTION__ , __LINE__, elapsed/1000, ##args); \
} while(0)

int main() {
   declare_timer;

   char *t = malloc(SIZE);

   for(size_t j = 0; j < 3; j++) {
      printf("Full:\n");
      for(size_t i = 0; i < SIZE - GB; i += GB) {
         start_timer {
            memset(&t[i], 0, GB);
         } stop_timer("[%lu]", i/GB);
      }
   }

   for(size_t j = 0; j < 10; j++) {
      uint64_t loc = rand() % (SIZE/GB);
      printf("Same %lu GB:\n", loc);
      for(size_t i = 0; i < 10; i++) {
         start_timer {
            memset(&t[loc*GB], 0, GB);
         } stop_timer("[%lu]", i);
      }
   }

   for(size_t j = 0; j < 10; j++) {
      uint64_t loc = rand() % (SIZE/GB);
      uint64_t val = rand();
      printf("New val same %lu GB:\n", loc);
      for(size_t i = 0; i < 10; i++) {
         start_timer {
            memset(&t[loc*GB], val, GB);
         } stop_timer("[%lu]", i);
      }
   }


   for(size_t j = 0; j < 10; j++) {
      uint64_t loc = rand() % (SIZE/GB);
      printf("New val change %lu GB:\n", loc);
      for(size_t i = 0; i < 10; i++) {
         uint64_t val = rand();
         start_timer {
            memset(&t[loc*GB], val, GB);
         } stop_timer("[%lu]", i);
      }
   }

   for(size_t j = 0; j < 3; j++) {
      printf("Full:\n");
      for(size_t i = 0; i < SIZE - GB; i += GB) {
         start_timer {
            memset(&t[i], 0, GB);
         } stop_timer("[%lu]", i/GB);
      }
   }
}

