#include <stdio.h>
#include <stdint.h>

#define rdmsr(msr,val1,val2) \
       __asm__ __volatile__("rdmsr" \
			    : "=a" (val1), "=d" (val2) \
			    : "c" (msr))
#define rdmsrl(msr,val) do { unsigned long a__,b__; \
       __asm__ __volatile__("rdmsr" \
			    : "=a" (a__), "=d" (b__) \
			    : "c" (msr)); \
       val = a__ | (b__<<32); \
} while(0)

#define wrmsr(msr,val1,val2) \
     __asm__ __volatile__("wrmsr" \
			  : /* no outputs */ \
			  : "c" (msr), "a" (val1), "d" (val2))
#define wrmsrl(msr,val) wrmsr(msr,(uint32_t)((uint64_t)(val)),((uint64_t)(val))>>32)


int main() {
   uint64_t val = 0x41412E; // UMask:0x41 + EVENT Select:0x2E + User bit + Enable bit
   uint64_t ret = 0x0;

   rdmsrl(0x187, ret); // 0x187 is mapped address of PERFEVTSEL1 MSR
   if ( ret != 0x41412E ) {
      wrmsrl(0x187, val);
   }

   rdmsrl(0xC2, ret);
   printf("%lu\n", ret);
   rdmsrl(0xC2, ret);
   printf("%lu\n", ret);
}
