#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/time.h>
#include <sys/mman.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <assert.h>
#include <linux/mman.h>
#include <iostream>
#include <chrono>
#include <omp.h>
#include <libpmem.h>
#define TBB_SUPPRESS_DEPRECATED_MESSAGES 1
#include "tbb/tbb.h"

#ifdef __x86_64__
#define rdtscll(val) {                                           \
       unsigned int __a,__d;                                        \
       asm volatile("rdtsc" : "=a" (__a), "=d" (__d));              \
       (val) = ((unsigned long)__a) | (((unsigned long)__d)<<32);   \
}
#else
#define rdtscll(val) __asm__ __volatile__("rdtsc" : "=A" (val))
#endif

#define TRASHING_CACHE_LINE_SIZE 64
#define GB ((size_t)1024*1024*1024)
#define LARGE_PAGE_SIZE (2LU*1024*1024)


static __uint128_t __thread g_lehmer64_state;
void init_seed(void) {
   g_lehmer64_state = rand();
}
uint64_t lehmer64() {
  g_lehmer64_state *= 0xda942042e4dd58b5;
  return g_lehmer64_state >> 64;
}

int main(int argc, char** argv) {
   size_t size = GB*atoi(argv[1]);

   tbb::task_scheduler_init init(10);

   printf("Size %lu\n", size);
   char *data;
   data = (char*)mmap(NULL, size, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS | MAP_HUGETLB, 0, 0);
   if(data == MAP_FAILED)
      printf("Cannot allocate gigantic pages, do echo 60 > /sys/devices/system/node/node0/hugepages/hugepages-1048576kB/nr_hugepages\n");
   //data = (char*)mmap(NULL, size, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS, 0, 0);
   //printf("WARNING, NOT USING LARGE PAGES, ONLY FOR HEMEM!\n");

   //const char *path = "/pmem2/test";
   //int fd = open(path,  O_RDWR | O_CREAT | O_DIRECT, 0777);
   //if(fd == -1)
   //printf("Cannot open %s\n", path);
   //fallocate(fd, 0, 0, size);
   //data = (char*)pmem_map_file(path, 0, 0, 0777, NULL, NULL);

   {
      auto starttime = std::chrono::system_clock::now();
      tbb::parallel_for(tbb::blocked_range<uint64_t>(0, size/4096), [&](const tbb::blocked_range<uint64_t> &range) {
            char *tmp = (char*)malloc(4096);
            for (uint64_t i = range.begin(); i != range.end(); i++) {
               memcpy(&data[i*4096], tmp, 4096);
            }
            free(tmp);
      });
      auto duration = std::chrono::duration_cast<std::chrono::microseconds>(
            std::chrono::system_clock::now() - starttime);
      printf("Init done %fs\n", duration.count() / 1000000.0);
   }
   //memset(data, 0, size);

   /*{
      auto starttime = std::chrono::system_clock::now();
      tbb::parallel_for(tbb::blocked_range<uint64_t>(0, size/4096), [&](const tbb::blocked_range<uint64_t> &range) {
            char *tmp = (char*)malloc(4096);
            for (uint64_t i = range.begin(); i != range.end(); i++) {
               memcpy(tmp, &data[i*4096], 4096);
            }
            free(tmp);
      });
      auto duration = std::chrono::duration_cast<std::chrono::microseconds>(
            std::chrono::system_clock::now() - starttime);
      printf("Clearing %fs\n", duration.count() / 1000000.0);
   }*/

   sleep(5);

   //omp_set_num_threads(20);
   //#pragma omp parallel
   {
      size_t nb_it = 300;
      size_t nb_req_per_it = 100LU*1024*1024;
      //size_t show = omp_get_thread_num() == 0;
      size_t show = 1;
      init_seed();
      for(size_t i = 0; i < nb_it; i++) {
         uint64_t a, b;
         uint64_t tmp;
         size_t *idx = (size_t*)malloc(nb_req_per_it*sizeof(size_t));
         for(size_t j = 0; j < nb_req_per_it; j++) {
            idx[j] = lehmer64()%size;
         }


         rdtscll(a);
         for(size_t j = 0; j < nb_req_per_it; j++) {
            //memcpy(tmp, &data[indexes[lehmer64()%nb_hot]], value_size);
            //memcpy(&data[indexes[lehmer64()%nb_hot]*value_size], tmp, value_size);
            data[lehmer64()%size]='a';
            //data[idx[j]]='a';
            //int tmp = data[lehmer64()%size];
            //int tmp = data[idx[j]];
         }
         rdtscll(b);
         if(show)
            printf("Cycles %lu\n", b -a);
         free(idx);
         //system("sudo env -i killall -w sleep");
      }
      //});
   }
      //system("sudo env -i killall -w sleep");
   return 0;
}
