/*
 *  hello-1.c - The simplest kernel module.
 */
#include <linux/module.h>	/* Needed by all modules */
#include <linux/moduleparam.h>
#include <linux/kernel.h>	/* Needed for KERN_INFO */
#include <linux/mm.h>
#include <linux/hugetlb.h>
#include <linux/perf_event.h>
#include <asm-generic/io.h>
#include <linux/timer.h>
#include <linux/sort.h>

#define LARGE_PAGE_SIZE (2LU*1024*1024) // 1GB
#define CACHE_SIZE (48*512) // DRAM is 64GB -> cache is 48GB on our machine
#define pfn_to_large_pfn(pfn) ((pfn)/(512)) // 512 4KB pages in a 2MB page
#define pfn_to_bucket(pfn) (pfn_to_large_pfn(pfn) % CACHE_SIZE)


static int max_conflicts = 0;
static u64 allocated_pages[CACHE_SIZE];


#define PEBS_SAMPLE_TYPE PERF_SAMPLE_IP | PERF_SAMPLE_TID | PERF_SAMPLE_ADDR
#define PMEM_READ 0x80d1
#define DRAM_READ 0x20d1
#define STORE_ALL 0x82d0

static int nb_samples = 0, nb_dram_loads = 0, nb_pmem_loads = 0, nb_stores = 0;
static struct perf_event **events;
static size_t configs[] = { DRAM_READ, PMEM_READ, STORE_ALL };

static u64 heatmap_load[CACHE_SIZE];
static u64 heatmap_store[CACHE_SIZE];
static u64 heatmap_snap[CACHE_SIZE];

#define PMEM_SIZE (512*512)
static u64 pagemap_dram[PMEM_SIZE];
static u64 pagemap_pmem[PMEM_SIZE];
static u64 pagemap_store[PMEM_SIZE];
static u64 pagemap_allocated[PMEM_SIZE];

#define MOVING_AVG_N 100
static struct timer_list my_timer;


static u64 predict_conflicts(struct page *page) {
   if(!allocated_pages[pfn_to_bucket(page_to_pfn(page))])
      return 0;
   return heatmap_snap[pfn_to_bucket(page_to_pfn(page))];
   //return allocated_pages[pfn_to_bucket(page_to_pfn(page))];
}

static void reserve_page(struct hstate *h, int nid, struct page *page)
{
   size_t bucket = pfn_to_bucket(page_to_pfn(page));
   allocated_pages[bucket]++;
   heatmap_snap[bucket] += 1000;
   pagemap_allocated[pfn_to_large_pfn(page_to_pfn(page))%PMEM_SIZE]++;

   list_move(&page->lru, &h->hugepage_activelist);
   set_page_count(page, 1);
   ClearHPageFreed(page);
   h->free_huge_pages--;
   h->free_huge_pages_node[nid]--;

   //printk("Allocating page %lu in bucket %lu on node %d, expected conflicts %llu\n", pfn_to_large_pfn(page_to_pfn(page)), bucket, nid, predict_conflicts(page));
}

static struct page *minimize_conflicts(struct hstate *h, int nid, struct vm_area_struct *vma, unsigned long addr)
{
   u64 conflicts, min_conflicts = 100000;
   struct page *page, *best_page = NULL;
   bool pin = !!(current->flags & PF_MEMALLOC_PIN);

   lockdep_assert_held(&hugetlb_lock);

   list_for_each_entry(page, &h->hugepage_freelists[nid], lru) {
      if (pin && !is_pinnable_page(page))
         continue;

      if (PageHWPoison(page))
         continue;

      //reserve_page(h, nid, page); //TODO
      //return page; // TODO!!!!!!!

      conflicts = predict_conflicts(page);
      if(conflicts < min_conflicts) {
         min_conflicts = conflicts;
         best_page = page;
      }
      if(min_conflicts == 0)
         break;
   }

   if(best_page)
      reserve_page(h, nid, best_page);
   return best_page;
}


static u64 perf_virt_to_phys(u64 virt)
{
	u64 phys_addr = 0;

	if (!virt)
		return 0;

	if (virt >= TASK_SIZE) {
		if (virt_addr_valid((void *)(uintptr_t)virt) &&
		    !(virt >= VMALLOC_START && virt < VMALLOC_END))
			phys_addr = (u64)virt_to_phys((void *)(uintptr_t)virt);
	} else {
		if (current->mm != NULL) {
			struct page *p;
			pagefault_disable();
			if (get_user_page_fast_only(virt, 0, &p)) {
				phys_addr = (page_to_pfn(p) << PAGE_SHIFT) + virt % PAGE_SIZE;
				put_page(p);
			}
			pagefault_enable();
		}
	}
	return phys_addr;
}

static void pebs_sample(struct perf_event *event, struct perf_sample_data *data, struct pt_regs *regs)
{
   size_t phys = perf_virt_to_phys(data->addr);
   size_t bucket = (phys / LARGE_PAGE_SIZE) % CACHE_SIZE;
   switch(event->attr.config) {
      case DRAM_READ:
         nb_dram_loads++;
         heatmap_load[bucket]++;
         pagemap_dram[(phys/LARGE_PAGE_SIZE)%PMEM_SIZE]++;
         break;
      case PMEM_READ:
         nb_pmem_loads++;
         heatmap_load[bucket]++;
         pagemap_pmem[(phys/LARGE_PAGE_SIZE)%PMEM_SIZE]++;
         break;
      case STORE_ALL:
         heatmap_store[bucket]++;
         pagemap_store[(phys/LARGE_PAGE_SIZE)%PMEM_SIZE]++;
         nb_stores++;
         break;
   }
   nb_samples++;
   //printk("Event %p (config %llx) CPU %u vs %u Tid %u Virt addr: %llx Phys %llx\n", event, event->attr.config, data->cpu_entry.cpu, smp_processor_id(), current->pid, data->addr, perf_virt_to_phys(data->addr));
}

static void add_stats(u64 total, u64 *array_priv, u64 *array_shared, u64 shared) {
   size_t idx = 0;
   while(total > 2) {
      total /= 2;
      idx++;
   }
   if(idx > 20)
      idx = 20;
   if(shared) {
      array_shared[idx]++;
   } else {
      array_priv[idx]++;
   }
}

#define HEAT_PRINT_SIZE 2048
static char *printbuf;
struct page_stat {
   int i;
   int samples;
   int shared;
} stats[CACHE_SIZE];

static int cmp_stat(const void *_a, const void *_b) {
   const struct page_stat *a = _a;
   const struct page_stat *b = _b;
   if(a->samples < b->samples)
      return 1;
   if(a->samples > b->samples)
      return -1;
   return 0;
}
static void swap_stat(void *a, void *b, int size) {
   struct page_stat tmp = *(struct page_stat*)a;
   *(struct page_stat*)a = *(struct page_stat*)b;
   *(struct page_stat*)b = tmp;
}


static void my_timer_callback(struct timer_list *timer) {
   int i, j, nb_prints = 0;
   u64 avg = 0, print_idx = 0;

   u64 shared_dram[20] = {};
   u64 shared_pmem[20] = {};
   u64 shared_store[20] = {};

   u64 priv_dram[20] = {};
   u64 priv_pmem[20] = {};
   u64 priv_store[20] = {};

   printk("Samples: %d DRAM %d PMEM %d STORE\n", nb_dram_loads, nb_pmem_loads, nb_stores);

   for(i = 0; i < CACHE_SIZE; i++) {
      u64 new = heatmap_load[i] + heatmap_store[i];
      u64 old = heatmap_snap[i];
      u64 moving_avg = old*(MOVING_AVG_N - 1)/(MOVING_AVG_N) + new;
      heatmap_snap[i] = moving_avg;
      heatmap_load[i] = 0;
      heatmap_store[i] = 0;
      avg += moving_avg;
   }

   for(i = 0; i < CACHE_SIZE; i++) {
      size_t nb_allocated = 0, nb_pages_with_samples = 0, avg_dram = 0, avg_pmem = 0, avg_store = 0;
      for(j = 0; j < PMEM_SIZE/CACHE_SIZE; j++) {
         size_t slot = j*CACHE_SIZE + i;
         if(pagemap_allocated[slot])
            nb_allocated++;
         if(pagemap_dram[slot] || pagemap_pmem[slot] || pagemap_store[slot])
            nb_pages_with_samples++;
         avg_dram += pagemap_dram[slot];
         avg_pmem += pagemap_pmem[slot];
         avg_store += pagemap_store[slot];
      }
      add_stats(avg_dram, priv_dram, shared_dram, nb_pages_with_samples > 1);
      add_stats(avg_pmem, priv_pmem, shared_pmem, nb_pages_with_samples > 1);
      add_stats(avg_store, priv_store, shared_store, nb_pages_with_samples > 1);

      stats[i].i = i;
      stats[i].samples = avg_dram + avg_pmem + avg_store;
      stats[i].shared = nb_pages_with_samples;
   }

   // Print some stats
   print_idx += snprintf(&printbuf[print_idx], HEAT_PRINT_SIZE - print_idx, "\nDRAM Shared:  ");
   for(i = 0; i < 20; i++)
      print_idx += snprintf(&printbuf[print_idx], HEAT_PRINT_SIZE - print_idx, "%llu ", shared_dram[i]);
   print_idx += snprintf(&printbuf[print_idx], HEAT_PRINT_SIZE - print_idx, "\nDRAM Priv:    ");
   for(i = 0; i < 20; i++)
      print_idx += snprintf(&printbuf[print_idx], HEAT_PRINT_SIZE - print_idx, "%llu ", priv_dram[i]);
   print_idx += snprintf(&printbuf[print_idx], HEAT_PRINT_SIZE - print_idx, "\nPMEM Shared:  ");
   for(i = 0; i < 20; i++)
      print_idx += snprintf(&printbuf[print_idx], HEAT_PRINT_SIZE - print_idx, "%llu ", shared_pmem[i]);
   print_idx += snprintf(&printbuf[print_idx], HEAT_PRINT_SIZE - print_idx, "\nPMEM Priv:    ");
   for(i = 0; i < 20; i++)
      print_idx += snprintf(&printbuf[print_idx], HEAT_PRINT_SIZE - print_idx, "%llu ", priv_pmem[i]);
   print_idx += snprintf(&printbuf[print_idx], HEAT_PRINT_SIZE - print_idx, "\nSTORE Shared: ");
   for(i = 0; i < 20; i++)
      print_idx += snprintf(&printbuf[print_idx], HEAT_PRINT_SIZE - print_idx, "%llu ", shared_store[i]);
   print_idx += snprintf(&printbuf[print_idx], HEAT_PRINT_SIZE - print_idx, "\nSTORE Priv:   ");
   for(i = 0; i < 20; i++)
      print_idx += snprintf(&printbuf[print_idx], HEAT_PRINT_SIZE - print_idx, "%llu ", priv_store[i]);
   print_idx += snprintf(&printbuf[print_idx], HEAT_PRINT_SIZE - print_idx, "\n");


   // Sort and see the biggest conflicts!!
   sort(stats, CACHE_SIZE, sizeof(struct page_stat), cmp_stat, swap_stat);
   for(i = 0; i < 10; i++) {
      print_idx += snprintf(&printbuf[print_idx], HEAT_PRINT_SIZE - print_idx, "Biggest %2d - %d samples\t", stats[i].i, stats[i].samples);
      for(j = 0; j < PMEM_SIZE/CACHE_SIZE; j++) {
         size_t slot = j*CACHE_SIZE + stats[i].i;
         print_idx += snprintf(&printbuf[print_idx], HEAT_PRINT_SIZE - print_idx, "%llu ", pagemap_dram[slot] + pagemap_pmem[slot] + pagemap_store[slot]);
      }
      print_idx += snprintf(&printbuf[print_idx], HEAT_PRINT_SIZE - print_idx, "\n");
   }

   for(i = 0; i < CACHE_SIZE; i++) {
      if(stats[i].shared <= 1)
         continue;
      print_idx += snprintf(&printbuf[print_idx], HEAT_PRINT_SIZE - print_idx, "Biggest Shared %2d - %d samples\t", stats[i].i, stats[i].samples);
      for(j = 0; j < PMEM_SIZE/CACHE_SIZE; j++) {
         size_t slot = j*CACHE_SIZE + stats[i].i;
         print_idx += snprintf(&printbuf[print_idx], HEAT_PRINT_SIZE - print_idx, "%llu ", pagemap_dram[slot] + pagemap_pmem[slot] + pagemap_store[slot]);
      }
      print_idx += snprintf(&printbuf[print_idx], HEAT_PRINT_SIZE - print_idx, "\n");
      nb_prints++;
      if(nb_prints >= 10)
         break;
   }

   // Print!
   printk(KERN_INFO "%s", printbuf);

   // Reset
   memset(pagemap_dram, 0, sizeof(pagemap_dram));
   memset(pagemap_pmem, 0, sizeof(pagemap_pmem));
   memset(pagemap_store, 0, sizeof(pagemap_store));
   nb_dram_loads = nb_pmem_loads = nb_stores = 0;

   mod_timer(&my_timer, jiffies + msecs_to_jiffies(1000));
}

int init_module(void)
{
   size_t config, cpu, ncpus = num_online_cpus();
   static struct perf_event_attr wd_hw_attr = {
      .type = PERF_TYPE_RAW,
      .size		= sizeof(struct perf_event_attr),
      .pinned		= 0,
      .disabled	= 1,
      .precise_ip = 2,
      .sample_id_all = 1,
      .exclude_kernel = 1,
      .exclude_guest = 1,
      .exclude_hv = 0,
      .exclude_user =0,
      .sample_type = PERF_SAMPLE_IP | PERF_SAMPLE_TID | PERF_SAMPLE_WEIGHT | PERF_SAMPLE_ADDR | PERF_SAMPLE_PHYS_ADDR,
   };

   memset(pagemap_allocated, 0, sizeof(pagemap_allocated));
   printbuf = vmalloc(HEAT_PRINT_SIZE);

   events = vmalloc(ncpus * ARRAY_SIZE(configs) * sizeof(*events));
   printk(KERN_INFO "Creating %lu events - %lu configs %lu cpus\n", ARRAY_SIZE(configs) * ncpus, ARRAY_SIZE(configs), ncpus);
   for(config = 0; config < ARRAY_SIZE(configs); config++) {
      for(cpu = 0; cpu < ncpus; cpu++) {
         size_t idx = config * ncpus + cpu;
         wd_hw_attr.config = configs[config];
         if(configs[config] == STORE_ALL)
            wd_hw_attr.sample_period = 1000000;
         else
            wd_hw_attr.sample_period = 100000;
         events[idx] = perf_event_create_kernel_counter(&wd_hw_attr, cpu, NULL, pebs_sample, NULL);
         if(IS_ERR(events[idx])) {
            printk(KERN_INFO "Could not create event %lu on cpu %lu\n", configs[config], cpu);
            return -1;
         }
         perf_event_enable(events[idx]);
      }
   }


   if(max_conflicts)
      set_dequeue_hook(minimize_conflicts);
   else
      set_dequeue_hook(minimize_conflicts);

   timer_setup(&my_timer, my_timer_callback, 0);
   mod_timer(&my_timer, jiffies + msecs_to_jiffies(1000));
	return 0;
}

void cleanup_module(void)
{
   size_t cpu, config, ncpus = num_online_cpus();
   set_dequeue_hook(NULL);
   del_timer(&my_timer);
	printk(KERN_INFO "Goodbye world nb_samples %d.\n", nb_samples);
   for(config = 0; config < ARRAY_SIZE(configs); config++) {
      for(cpu = 0; cpu < ncpus; cpu++) {
         size_t idx = config * ncpus + cpu;
         perf_event_disable(events[idx]);
         perf_event_release_kernel(events[idx]);
      }
   }
   vfree(printbuf);
}

module_param(max_conflicts, int, S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH);
MODULE_PARM_DESC(max_conflicts, "Set to 1 to maximize conflicts");
MODULE_LICENSE("GPL");
