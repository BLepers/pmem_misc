#ifndef _GNU_SOURCE
#define _GNU_SOURCE
#endif
#define __USE_GNU
#include <stdio.h>
#include <stdint.h>
#include <pthread.h>
#include <assert.h>
#include <string.h>
#include <unistd.h>
#include <stdlib.h>
#include <dlfcn.h>
#include <sys/sysinfo.h>
#include <sys/time.h>
#include <execinfo.h>
#include <new>
#include <sys/types.h>
#include <sys/mman.h>
#include <linux/mman.h>

static void *(*libc_mmap)(void *, size_t, int, int, int, off_t);
static void *(*libc_mmap64)(void *, size_t, int, int, int, off_t);
static int (*libc_munmap)(void *, size_t);

#define LARGE_PAGE_SIZE (2LU*1024*1024)
size_t round_up(size_t size) {
   size_t mod = size % LARGE_PAGE_SIZE;
   if(mod)
      size = size/LARGE_PAGE_SIZE*LARGE_PAGE_SIZE+LARGE_PAGE_SIZE;
   return size;
}

extern "C" void *mmap(void *start, size_t length, int prot, int flags, int fd, off_t offset) {
   if(flags & MAP_ANONYMOUS) {
      length = round_up(length);
      flags |= MAP_HUGETLB;
   }
   void *addr = libc_mmap(start, length, prot, flags, fd, offset);
   memset(addr, 0, length);
   return addr;
}

extern "C" void *mmap64(void *start, size_t length, int prot, int flags, int fd, off_t offset) {
   if(flags & MAP_ANONYMOUS) {
      length = round_up(length);
      flags |= MAP_HUGETLB;
   }
   void *addr = libc_mmap64(start, length, prot, flags, fd, offset);
   memset(addr, 0, length);
   return addr;
}

extern "C" int munmap(void *start, size_t length) {
   int addr = libc_munmap(start, length);
   return addr;
}


void __attribute__((constructor)) m_init(void) {
   libc_mmap = (void * ( *)(void *, size_t, int, int, int, off_t)) dlsym(RTLD_NEXT, "mmap");
   libc_munmap = (int ( *)(void *, size_t)) dlsym(RTLD_NEXT, "munmap");
   libc_mmap64 = (void * ( *)(void *, size_t, int, int, int, off_t)) dlsym(RTLD_NEXT, "mmap64");
}
